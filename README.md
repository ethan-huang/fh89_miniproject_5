# Mini Project 5: Serverless Rust Microservice and AWS DynamoDB

## Project Description

This project is a serverless microservice written in Rust, designed to interact with AWS DynamoDB. The service is designed to be scalable, reliable, and easily deployable as an AWS Lambda function, providing an API endpoint for retrieving student details based on a given student ID.

## Goals

- Create a Rust AWS Lambda function
- Implement a simple service
- Connect to a database

## DynamoDB Table Structure

- name of Table: DukePerson
- with following fields:
- id(key) : String
- name : String
- major : String
- degree : String

## Steps

0. create a new user for this project
1. `cargo lambda new <Your Project Name> && cd <Your Project Name>`
2. change the main.rs file and Cargo.toml
3. I personally implemented the method of PUT
4. Run 'cargo build --release' to build the application
5. Run 'cargo deploy --region<region> --iam-role<your-role>' to deploy the function
6. Added an API GATEWAY for the lambda function, and add 'AmazonDynamoDBFullAccess', 'AWSLambda_FullAccess', 'AWSLambdaBasicExecutionRole' for the cargo role under the permissions tab.
7. Go to the test console to put a new DukePerson to the database.

## Screenshots

- ![new user](/images/iamuser.png)

- ![build & watch](/images/watch.png)

- ![add APIGateway and test](/images/upload.png)

- ![database update](/images/result.png)
