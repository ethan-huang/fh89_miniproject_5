use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use simple_logger::SimpleLogger;
use std::collections::HashMap;
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};

#[derive(Deserialize)]
struct Request {
    id: Option<String>,
    name: Option<String>,
    major: Option<String>,
    degree: Option<String>, 
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init()?;
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    post_new_info(&client, request.id, request.name, request.major, request.degree).await?;
    Ok(json!({"status": "success"}))
}

async fn post_new_info(client: &Client, id: Option<String>, name: Option<String>, major: Option<String>, degree: Option<String>) -> Result<(), LambdaError> {
    let table_name = "DukePerson";
    let mut item: HashMap<String, AttributeValue> = HashMap::new();
    
    let id_av = AttributeValue::S(id.expect("REASON"));
    let name_av = AttributeValue::S(name.expect("REASON"));
    let major_av = AttributeValue::S(major.expect("REASON"));
    let degree_av = AttributeValue::S(degree.expect("REASON"));

    client.put_item()
        .table_name(table_name)
        .item("id", id_av)
        .item("name", name_av)
        .item("major", major_av)
        .item("degree",degree_av)
        .send()
        .await?;

    Ok(())
}
